package com.indonesia.ridwan.buttonbinding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.indonesia.ridwan.buttonbinding.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    Model creatorbe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        creatorbe = new Model("Test1", "Test2");
        binding.setCreatorbe(creatorbe);
    }

    public void onButtonClick(View view) {
        Toast.makeText(this, "di klik oi", Toast.LENGTH_LONG).show();

        creatorbe.setFirstName("Namaku Ridwan hasanah");

        creatorbe.setLastName("creatorbe");
    }

}
