package com.indonesia.ridwan.buttonbinding;

/**
 * Created by hasanah on 9/10/16.
 */

import android.databinding.BaseObservable;
import android.databinding.Bindable;

public class Model extends BaseObservable {
    private String firstName;
    private String lastName;

    public Model(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Bindable
    public String getFirstName() {
        return this.firstName;
    }

    @Bindable
    public String getLastName() {
        return this.lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(com.indonesia.ridwan.buttonbinding.BR.firstName);
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        notifyPropertyChanged(com.indonesia.ridwan.buttonbinding.BR.lastName);
    }
}